---
#gitlab
- name: install python-pyOpenSSL
  package:
    name: 'python-openssl'
    state: latest
  
- name: create directories
  file:
    path: '{{ item.path }}'
    state: directory
    owner: '{{ item.owner }}'
    group: '{{ item.group }}'
    mode: '{{ item.mode }}'
  with_items:
    - '{{ gitlab_docker }}'
    - '{{ gitlab_config }}'
    - '{{ gitlab_logs }}'
    - '{{ gitlab_data }}'
    - '{{ gitlab_ssl }}'
    - '{{ gitlab_backup }}'

- name: generate an openssl private key
  openssl_privatekey:
    path: '{{ gitlab_ssl.path }}/{{ ansible_host }}.key'
    force: True

- name: generate an openssl certificate signing request
  openssl_csr:
    path: '{{ gitlab_ssl.path }}/{{ ansible_host }}.csr'
    privatekey_path: '{{ gitlab_ssl.path }}/{{ ansible_host }}.key'
    common_name: Test site
    country_name: Unknown
    organization_name: Unknown
    organizational_unit_name: Unknown
    force: True

- name: generate a self signed openssl certificate
  openssl_certificate:
    path: '{{ gitlab_ssl.path }}/{{ ansible_host }}.crt'
    privatekey_path: '{{ gitlab_ssl.path }}/{{ ansible_host }}.key'
    csr_path: '{{ gitlab_ssl.path }}/{{ ansible_host }}.csr'
    provider: selfsigned
    force: True

- name: download gitlab image
  docker_image:
    name: '{{ gitlab_docker_image_publisher }}/{{ gitlab_docker_image }}:{{ gitlab_docker_image_version }}'

- name: start gitlab container
  docker_container:
    name: '{{ gitlab_name }}'
    hostname: '{{ ansible_host }}'
    image: '{{ gitlab_docker_image_publisher }}/{{ gitlab_docker_image }}:{{ gitlab_docker_image_version }}'
    state: started
    restart_policy: unless-stopped
    volumes:
      - '{{ gitlab_config.path }}:/etc/gitlab'
      - '{{ gitlab_logs.path }}:/var/log/gitlab'
      - '{{ gitlab_data.path }}:/var/opt/gitlab'
      - '{{ gitlab_ssl.path }}:/etc/gitlab/ssl'
    published_ports:
      - '{{ gitlab_port_22 }}:22'
      - '{{ gitlab_port_80 }}:80'
      - '{{ gitlab_port_443 }}:443'
    env:
      GITLAB_OMNIBUS_CONFIG: "external_url 'https://{{ ansible_host }}:{{ gitlab_port_443 }}'; nginx['redirect_http_to_https'] = true; nginx['ssl_certificate_key'] = '/etc/gitlab/ssl/{{ ansible_host }}.key'; nginx['ssl_certificate'] = '/etc/gitlab/ssl/{{ ansible_host }}.crt'; gitlab_rails['gitlab_shell_ssh_port'] = '{{ gitlab_port_22 }}'"

- name: schedule nightly backup of {{ gitlab_name }}
  cron:
    name: 'nightly backup of {{ gitlab_name }}'
    #3:05am avoids daylight savings conflicts
    minute: '5'
    hour: '3'
    #8th-14th avoids most change freezes
    day: '*'
    month: '*'
    weekday: '*'
    #only executes Mondays
    job: "docker exec -t {{ gitlab_name }} gitlab-rake gitlab:backup:create"
