#data structure model

/locdrv/
		backups/
			hostname/
				hostname-backup-date.tar.lz4
		dataloc/
			nativeapp1/
			nativeapp2/
		datadock/
			samba/
				conf/
			gitlab/
				data/
			mysql/
				data/
		datashr/
			files/
				isos/
				movies/
				vm templates/
			swap/
		vms/
			ubuntu1/
			ubuntu2/
			centos1/
			centos2/
